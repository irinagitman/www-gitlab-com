#############################################################
# Key Performance Indicators
# This section contains only KPIs
# Ordering the same way it's represented in the handbook.
#############################################################

- name: MRARR
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: MRARR (pronounced "mer-arr," like a pirate) is the measurement of Merge Requests from customers, multiplied with
    the revenue of the account (ARR) from that customer. This measures how active our biggest customers are contributing to GitLab.
    We believe the higher this number the better we'll retain these customers and improve product fit for large enterprises.
    The unit of MRARR is MR Dollars (MR$). MR Dollars is different than the normal Dollars which is used for ARR.
    We are tracking current initiatives to improve this in this <a href="https://gitlab.com/groups/gitlab-com/-/epics/1225">epic</a>.
  target: Greater than 20M MR Dollars per month
  org: Quality Department
  is_key: true
  health:
    level: 1
    reasons:
      - Limited visibility, dashboard currently in SAFE Sisense area
      - Collaborating with Community team to drive contribution efficiency
      - Learned - Previous hackathon did not add momentum to customer contributions, also low activity in same period last year
      - Next - Create training for TAMs, build developer certification, hire Community Outreach team
  urls:
    - https://app.periscopedata.com/app/gitlab:safe-dashboard/919308/MRARR-Public-Dashboard?widget=12668727&udv=0


- name: Open Community MR Age
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: OCMA (pronounced "ock-mah") measures the median time of all open MRs as of a specific date.
    In other words, on any given day, this calculates the number of open MRs and median time in an open state for open MRs at that point in time.
  target: Below 100 days
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
      - This metric is new with an initial target of below 100 days
      - Expanded stale merge request reports and taking action based on age and mergeability
  sisense_data:
    chart: 11850655
    dashboard: 872455
    embed: v2
    shared_dashboard: a4b32a25-f3dc-46b2-8d74-a69101cb4e04
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12325

- name: GitLab project master pipeline success rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of the GitLab project master branch success rate. A key indicator to the stability of our releases.
     We will continue to leverage <a href="https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/195">Merge Trains</a> in this effort.
  target: Above 95%
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/kwiebers">Kyle Wiebers</a>
  health:
    level: 3
    reasons:
      - Maintaining above 90%
  sisense_data:
    chart: 8573283
    dashboard: 516343
    embed: v2

- name: Review App deployment success rate for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of our test tooling to enable engineering efficiency.
  target: Above 99%
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/kwiebers">Kyle Wiebers</a>
  health: 
    level: 2
    reasons:
      - Stability improving to ~90%
      - Stabilized kubernetes environment to support end to end smoke testing.
  urls:
    - https://gitlab.com/groups/gitlab-org/-/epics/605
    - https://gitlab.com/groups/gitlab-org/-/epics/606
    - https://gitlab.com/groups/gitlab-org/-/epics/6024
    - https://gitlab.com/gitlab-org/gitlab/-/issues/340837
  sisense_data:
    chart: 6721558
    dashboard: 516343
    embed: v2

- name: Time to First Failure
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: TtFF (pronounced "teuf") measures the average time from pipeline creation until the first actionable failed build is completed for the GitLab monorepo project.
    The Quality Department <a href="https://youtu.be/6dK5j8ADa3U?t=573">recently discussed</a> the desire to measure the average time to first failure.
    We want to run the tests that are likely to fail first and shorten the feedback cycle to R&D teams.
  target: Below 15 minutes
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/kwiebers">Kyle Wiebers</a>
  health:
    level: 2
    reasons:
      - Reset target to 15 after adjusting KPI for time to first failure.
      - Investigating and adding diagnostic charts to understand increase in last two months
  urls:
    - https://gitlab.com/gitlab-org/gitlab/-/issues/333857
  sisense_data:
    chart: 11955006
    dashboard: 878780
    shared_dashboard: 9e320ebb-43f6-4b30-9f72-7f02636ae410
    embed: v2

- name: S1 OBA
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: S1 Open Bug Age (OBA) measures the total number of days that all S1 bugs are open within a month divided by the number of S1 bugs within that month.
  target: Below 150 days
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/tpazitny">Tanya Pazitny</a>
  health:
    level: 3
    reasons:
      - Updating target from 150 to 100 days to stay ambitious
  sisense_data:
    dashboard: 856737
    chart: 11997705
    shared_dashboard: ae618b53-194d-40ed-a3ca-7f0d4ca9a492
    embed: v2

- name: S2 OBA
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: S2 Open Bug Age (OBA) measures the total number of days that all S2 bugs are open within a month divided by the number of S2 bugs within that month.
  org: Quality Department
  target: Below 300 days
  is_key: true
  dri: <a href="https://gitlab.com/tpazitny">Tanya Pazitny</a>
  health:
    level: 2
    reasons:
    - Number of S2s are decreasing but average age is increasing
    - Our focus on reliability has positively impacted the close rate of recently created S2s
    - 25% of open S2s have been open for more than 2 years; we will focus on this area in the next refinement
  sisense_data:
    dashboard: 856747
    chart: 11997719
    shared_dashboard: 0ca65051-9c59-4cac-9d2b-e2077296d2a7
    embed: v2

- name: Quality Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab.
    Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Quality Department
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - Above target
  urls:
    - "https://app.periscopedata.com/app/gitlab/862331/Quality-Department-Retention"

- name: Quality Average Age of Open Positions
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-vacancy-time-to-fill"
  definition: Measures the average time job openings take from open to close. Positions are closed when offer is accepted.
    This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: Quality Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
      - Just established baseline
      - Decreased due to us offering higher compensation than previous
  sisense_data:
      chart: 11885848
      dashboard: 872394
      embed: v2
      filters:
          - name: Division
            value: Engineering
          - name: Department
            value: Quality

- name: Software Engineer in Test Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Software Engineers in Test against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#software-engineer-in-test">gearing ratio</a>
    We have a detailed <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vRM5jmGgT5H1kDOi6UwHTbK7PnoPYTYATnLe5HgVkJqe2VvaWo9fKpbnB6gR4vhx3UDby4wUeGwPYEq/pubhtml?gid=2059771943&single=true">gearing prioritization model</a> that informs important us which group we will hire an SET first.
  target: At 46 Software Engineers in Test
  org: Quality Department
  is_key: true
  dri: <a href="https://gitlab.com/tpazitny">Tanya Pazitny</a>
  health:
    level: 1
    reasons:
      - Added 2 hires since last review
      - Successful at hiring open roles, need to update chart to reflect gearing priority
      - Learned - Realized better alignment with Product with updated gearing priorization
      - Next - Will improve when current reqs are filled, total 11 additions with 7 from NYY
  sisense_data:
    chart: 9668867
    dashboard: 516343
    embed: v2

#############################################################
# Regular Performance Indicators
# This section contains only PIs
# This is grouped by the following order
#   - Engineering Productivity
#   - Community Outreach
#   - Quality Engineering
#   - Engineering Analytics (TBD)
#   - Department Efficiency
#   - People Metrics
# Please grouped PIs together for ease of review
#############################################################

############################
# Engineering Productivity
############################

- name: GitLab project average successful merge request pipeline duration
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average successful duration the GitLab project merge request pipelines. Key building block to improve
    our cycle time, and effiency. More <a href="https://gitlab.com/groups/gitlab-org/-/epics/1853">pipeline improvements</a>
    and <a href="https://gitlab.com/groups/gitlab-org/-/epics/5987">critical code paths</a> planned.
  target: Below 45 minutes
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Selective test execution is shortening cycle time by running most applicable backend and frontend tests
  sisense_data:
    chart: 6782964
    dashboard: 516343
    embed: v2

- name: GitLab project average pipeline cost per merge request
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure the total pipeline cost per merge request to measure engineering efficiency. This
    is calculated by taking the total cost of all pipelines divided by the number of merge requests.
  target: Below 7.50
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Cost decrease from efficiency gains via shift to n2d-standard-2 runners
      - Pipeline cost improvement of 5% from implementing static check in rspec pileines.
      - Experimenting with limited rspec tests in 14.1
  sisense_data:
    chart: 11689663
    dashboard: 862651
    embed: v2
    shared_dashboard: a6ec2792-d044-411d-aca7-f4496543a461

############################
# Community Outreach
############################

- name: Community MR Coaches per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: The number of MR Coaches defined by team.yml role
  target: Above 50 coaches per month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Growing MR coach specialties across Engineering job specialties (Development, Quality, UX)
  sisense_data:
    chart: 9721107
    dashboard: 729542
    embed: v2
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10519

- name: Unique Community Contributors per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Distribution of unique authors for all merged Community contribution MRs by month.
  target: Above 150 contributors per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - Working with Community team to create sustainable practices to streamline review
      - September Hackathon historically leads to increase in contributors
  sisense_data:
    chart: 9522755
    dashboard: 729542
    embed: v2

- name: Percent of Feature Community Contribution MRs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Percentage of Community Contributions that are related to feature development
  target: Above 30%
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
      - Made progress of hitting goal of 30% by improving Community Contribution automation and improve issue hygiene and refinement for Community Contributors
  sisense_data:
    chart: 9640193
    dashboard: 729542
    embed: v2

- name: Percent of MRs from Community
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: This is the ratio of community contributions to the number of merged product MRs. As we grow as a company we want to make sure the community scales with the company.
  target: Above 8% of all MRs
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Set target to 8% to start
      - Trend spikes with quarterly Hackathon
  sisense_data:
    chart: 11016270
    dashboard: 824124
    shared_dashboard: 803b4ecb-7152-42a1-ae93-b01963edad62
    embed: v2

############################
# Quality Engineering
############################

- name: Average duration of end-to-end test suite
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the speed of our full QA/end-to-end test suite in
    the <code>master</code> branch. A Software Engineering in Test job-family performance-indicator.
  target: TBD
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/tpazitny">Tanya Pazitny</a>
  health:
    level: 2
    reasons:
    - This metric is new with an initial target of below 60 minutes
  sisense_data:
    chart: 12908180
    dashboard: 912649
    embed: v2
    shared_dashboard: 997e89ce-7fe5-418a-8e6b-f94ebb674a35
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/198

- name: Average age of quarantined end-to-end tests
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability and effectiveness of our QA/end-to-end tests
    running in the <code>master</code> branch. A Software Engineering in Test job-family performance-indicator.
  target: TBD
  org: Quality Department
  is_key: false
  dri: <a href="https://gitlab.com/tpazitny">Tanya Pazitny</a>
  health:
    level: 2
    reasons:
    - This metric is new with an initial target of below 100 days
  sisense_data:
    chart: 12909850
    dashboard: 933059
    embed: v2
    shared_dashboard: 7f108f02-6a44-45fa-9cb0-fad6f09c5316
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/199

############################
# Engineering Analytics
############################

# TBD

############################
# Department Efficiency
############################

- name: Quality Budget Plan vs Actuals
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-budget-plan-vs-actuals"
  definition: We need to spend our investors' money wisely. We also need to run a responsible business to be successful, and to one day go on the public market.
    <a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11464">Latest data is in Adaptive, data team importing to Sisense in FY22Q2</a>
  target: See Sisense for target
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
      - There is currently a data lag to resolve
  urls:
  - https://app.periscopedata.com/app/gitlab/633240/Quality-Non-Headcount-BvAs

- name: Quality Handbook MR Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-merge-request-rate"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching <code>/source/handbook/engineering/quality/**</code> over time.
  target: Above 1 MR per person per month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Under target, but improved in August
      - Working to update team pages to make them better organized and more useful
  sisense_data:
    chart: 10586749
    dashboard: 621059
    shared_dashboard: 3f0e7d87-fc13-44c3-bbba-1a9feb48c3bd
    embed: v2

- name: Quality MR Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: Quality Department MR rate a key indicator showing how many changes the Quality Department implements directly in the GitLab product.
    It is important because it shows our iterative productivity based on the average MR merged per team member.
    We currently count all members of the Quality Department (Director, EMs, ICs) in the denominator because this is a team effort.
    The full definition of MR Rate is linked in the url section.
  target: Above 10 MRs per Month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Upgraded to attention since we hit our target of 10 in July 2020
    - Working on increasing Quality codebase maintainers and champion smaller iterations
  urls:
    - "/handbook/engineering/metrics/#merge-request-rate"
    - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/534
  sisense_data:
    chart: 8467527
    dashboard: 654023
    shared_dashboard: 4278f770-7709-4a5e-89f7-812a319c2fbb
    embed: v2

############################
## People Metrics
############################

- name: Quality Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor by function and department so managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in low cost areas.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Recent upward trend is due to geo location factor revision. No new hires during uptick.
      - We are focused on location factors of our new hires and vacancies and targetting efficient locations globally.
      - We are considering adjusting to 0.66 to reflect the division KPI.
  sisense_data:
    chart: 7045895
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"

- name: Quality New Hire Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've fluctuated above and below the target line recently, which for a small department is not worrisome.
    - We are considering adjusting to 0.66 to reflect the division KPI.
  sisense_data:
    chart: 9389208
    dashboard: 719540
    embed: v2

- name: Quality Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
      - Metric is new and is being monitored
  sisense_data:
    chart: 11860231
    dashboard: 873087
    embed: v2
    filters:
      - name: Breakout
        value: Department
      - name: Breakout_Division_Department
        value: Engineering - Quality

- name: Quality Department Discretionary Bonus Rate
  base_path: "/handbook/engineering/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Metric is new and is being monitored
      - We've fluctuated above and below the target line recently, which for a small department is not worrisome.
  sisense_data:
    chart: 11860249
    dashboard: 873088
    embed: v2
    filters:
      - name: Breakout
        value: Department
      - name: Breakout_Division_Department
        value: Engineering - Quality

- name: Engineering Productivity Engineer Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Engineering Productivity Engineers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 16 Engineering Productivity Engineers
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Currently at 25% of target
  sisense_data:
    chart: 9669589
    dashboard: 516343
    embed: v2
